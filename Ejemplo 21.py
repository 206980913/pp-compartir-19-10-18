
horas = int(input("Ingrese las horas laboradas: "))
salHora = int(input("Ingrese el salario por hora: "))

salBruto = horas * salHora

if salBruto < 100000:
    salMenor = salBruto * 0.9
    print(salMenor)

elif salBruto >= 100000:
    montoRestante = salBruto - 100000
    montoFijo = 100000
    salMayor = (montoFijo * 0.9) + (montoRestante * 0.85)
    print(salMayor)
