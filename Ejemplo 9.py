
print("Le recordamos que el maximo de entradas por persona son 4")

valorEntrada = 1000

cantEntradas = int(input("\nIngrese la cantidad de entradas: "))

if cantEntradas > 4 or cantEntradas == 0:
    print("\nCompra invalidad solo se permite una compra maxima de 4 entradas por persona o mas de 0")

elif cantEntradas == 1:
    print("\nMonto a pagar:", valorEntrada)

elif cantEntradas == 2:
    print("\nMonto a pagar:", (valorEntrada * 0.90) * cantEntradas)

elif cantEntradas == 3:
    print("\nMonto a pagar:", (valorEntrada * 0.85) * cantEntradas)

elif cantEntradas == 4:
    print("\nMonto a pagar:", (valorEntrada * 0.80) * cantEntradas)
