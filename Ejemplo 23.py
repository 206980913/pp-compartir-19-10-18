
categoria = int(input("Ingrese la categoria: "))
salario = int(input("Ingrese el salario: "))

if categoria < 1 or categoria > 3:
    print("Categoria erronea favor ingrese una categoria entre 1 y 3")
else:
    if categoria == 1:
        salario = salario + (salario * 0.15)
        print(salario)

    elif categoria == 2:
        salario = salario + (salario * 0.20)
        print(salario)

    elif categoria == 3:
        salario = salario + (salario * 0.25)
        print(salario)
