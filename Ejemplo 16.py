
import math

numero = int(input("Ingrese el numero que desea analizar: "))

if numero == 0:
    print("Favor digitar valores diferentes a 0")
else:
    if numero > 0:
        raiz = math.sqrt(numero)
        print(raiz)
    else:
        cuadrado = pow(numero, 2)
        cubo = pow(numero, 3)
        print(cuadrado, " ",cubo)
