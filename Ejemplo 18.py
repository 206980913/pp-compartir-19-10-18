
numero = int(input("Ingrese el numero que desea analizar: "))

if numero == 0:
    print("Favor digitar valores diferentes a 0")
else:
    if numero > 0:
        print("El numero,", numero, "es positivo")
    else:
        print("El numero,", numero, "es negativo")
