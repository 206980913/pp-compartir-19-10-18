for i in "HOLA MUNDO":
    print(i, end="")

print("\n")


for i in range (10):
    print(i * 2, " - ", end="")

print("\n")

for i in range ( 15, 10, -1):
    print(i, " - ", end="")

print("\n")


num = 0

while num <=3:
    print(num, " - ", end="")
    num +=1

print("\n")


num2 = 10

while num2 >0:
    print(num2, " - ", end="")
    num2 -= 1
