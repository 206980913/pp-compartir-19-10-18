
mensajeEntrada = "programa de geeracion de tablas de multiplicar.".capitalize() # capitalize hace la primera letra mayuscula

print(mensajeEntrada.center(60,"="))



#opcion = 1
opcion = 's'


#while opcion != 0:
while opcion != 'n': #cuando la opcion sea diferente de 1 haga
    tablaInicio = int(input(chr(27) + "[1;34m" + "ingrese la tabla inicial: "))
    tablaFinal = int(input("ingrese la tabla final: "))

    while tablaFinal < tablaInicio: #mientras la tabla final sea menor a la tabla inicial haga.
        print("La tabla de inicio debe ser menor a la tabla final")
        tablaInicio = int(input("ingrese la tabla inicial: "))
        tablaFinal = int(input("ingrese la tabla final: "))

    rangoInicio = int(input("ingrese el rango inicial: "))
    rangoFinal = int(input("ingrese el rango final: "))

    while rangoFinal < rangoInicio: #mientras el rango final sea menor al rango inicial haga.
        print("El rango de inicio debe ser menor a la tabla final")
        rangoInicio = int(input("ingrese el rango inicial: "))
        rangoFinal = int(input("ingrese el rango final: "))

    while tablaInicio < tablaFinal: #mientras la tabla final sea menor a la tabla inicial haga.

        for i in range(tablaInicio, tablaFinal+1): #recorrido de la tabla basado en la posicion de tablaInicio y tablaFinal
            #y se le suma 1 para que inicie en el valor que se ingreso.

            if i == 4: #si la posicion i == 4 haga
                print("la tabla (4) no la imprimo porque no quiero.".format(i))
                continue # continuar con lo siguiente
            for j in range(rangoInicio, rangoFinal+1):
                if j == 5 :
                    print("mas de 5 no quiero.")
                    break
                resutado = i * j
                #print("Multiplicar", i,"y", j, "es igual a:", resutado)
                print("Multiplicar %d * %d es igual a: %d"%(i,j,resutado)) #manera diferente para mostrar escritura

        tablaInicio += 1 #aumenta en 1 el recorrido
    opcion = (input("Desea ejecutar nuevamente el proceso: \n"
                           "s: Continuar\n"
                           "n: Salir \t"))
    if opcion == 'n' or opcion == 'N': #rompe el siclo de forma abrupta
        break
else:
        print("\nGracias por jugar.")

